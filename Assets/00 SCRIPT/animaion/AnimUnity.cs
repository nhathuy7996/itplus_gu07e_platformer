using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimUnity : AnimControllerBase
{
    Animator Anim;

    private void Awake()
    {
        Anim = this.GetComponent<Animator>();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public override void ChangeAnim(PlayerController.PlayerState state)
    {
        //base.ChangeAnim(state);

        for (int i = 0; i< (int)PlayerController.PlayerState.len; i++ )
        {
           if( (PlayerController.PlayerState)i == state)
            {
                Anim.SetBool( ((PlayerController.PlayerState)i).ToString(), true);
            }
            else
            {
                Anim.SetBool(((PlayerController.PlayerState)i).ToString(), false);
            }
        }
    }

}
