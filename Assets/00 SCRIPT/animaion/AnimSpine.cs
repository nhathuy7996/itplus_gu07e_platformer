using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

public class AnimSpine : AnimControllerBase
{
    SkeletonAnimation Anim;
    [SerializeField] [SpineAnimation] string idle, run, jump;

    private void Awake()
    {
        Anim = this.GetComponent<SkeletonAnimation>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void ChangeAnim(PlayerController.PlayerState state)
    {
        //base.ChangeAnim(state);

        switch (state)
        {
            case PlayerController.PlayerState.IDLE:
                Anim.AnimationState.SetAnimation(0,idle,true);
                break;

            case PlayerController.PlayerState.RUN:
                Anim.AnimationState.SetAnimation(0, run, true);
                break;
            case PlayerController.PlayerState.JUMP:
                Anim.AnimationState.SetAnimation(0, jump, false);
                break;
            case PlayerController.PlayerState.ON_AIR:
                Anim.AnimationState.SetAnimation(0, jump, false);
                break;

        }

    }
}
