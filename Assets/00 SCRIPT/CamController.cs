using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamController : MonoBehaviour
{
    [SerializeField]
    Rigidbody2D player;
    [SerializeField]
    Vector3 pos;
    [SerializeField] float Speed = 10;
    //[SerializeField] float zoom = 10;
    private void Awake()
    {

        pos = this.transform.position - player.transform.position;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       

        //this.GetComponent<Camera>().orthographicSize = zoom;
        if (player.velocity.x > 0)
        {
            pos.x = Mathf.Abs(pos.x);
        }
        else if (player.velocity.x < 0)
        {
            pos.x = -Mathf.Abs(pos.x);
        }

        Vector3 target_pos = pos + player.transform.position;
        target_pos.z = this.transform.position.z;


        this.transform.position = Vector3.Lerp(this.transform.position, target_pos, Speed );

    }

    
}
