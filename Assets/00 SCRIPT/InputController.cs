using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{

    public static int getAxisRawHorizon()
    {
        return (int)Input.GetAxisRaw("Horizontal");
    }
}
