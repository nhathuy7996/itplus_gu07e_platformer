using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    [SerializeField] PathCreator path;
    List<Vector3> ListPonits = new List<Vector3>();
    [SerializeField] float speed;

    Vector2 current_point, target_point;

    [SerializeField]
    int targetIndex = 0;

    private void Awake()
    {

        ListPonits = path.getPoints();
    }
    // Start is called before the first frame update
    void Start()
    {
       
      
        if (ListPonits.Count < 2)
            return;
        
        current_point = ListPonits[0];
        target_point = ListPonits[1];
        targetIndex = 1;
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position = Vector2.MoveTowards(this.transform.position, target_point, speed * Time.deltaTime);
        if(Vector2.Distance(this.transform.position, target_point) <= 0.5f)
        {
            changeTarget();
        }
    }

    void changeTarget()
    {
        if (targetIndex >= ListPonits.Count - 1)
        {
            targetIndex = 0;
           
        }else
            targetIndex++;

        target_point = ListPonits[targetIndex];
    }



}
