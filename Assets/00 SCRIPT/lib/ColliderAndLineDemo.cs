using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour
{
    [SerializeField] bool isOnGround = false;
    Rigidbody2D rigi;
    [SerializeField] float jumpForce;
    [SerializeField] float rayDetectGround;

    Collider2D colli;
    LineRenderer Line;
    [SerializeField] float lineLength;
    [SerializeField] float angle;

    // Start is called before the first frame update
    void Start()
    {
        rigi = this.GetComponent<Rigidbody2D>();
        colli = this.GetComponent<BoxCollider2D>();
        Line = this.GetComponent<LineRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Space) && isOnGround)
        {
            rigi.AddForce(Vector2.up * jumpForce);
            isOnGround = false;
        }

        //Debug.DrawRay(this.transform.position, Vector2.down * rayDetectGround, Color.red);
        //RaycastHit2D hit = Physics2D.Raycast(this.transform.position, Vector2.down, rayDetectGround);

        //if(hit.collider != null)
        //    isOnGround = true;
        DetectGround();
        drawLine();
    }

    void DetectGround()
    {
        RaycastHit2D[] hits = new RaycastHit2D[100];
        colli.Cast(Vector2.down, hits, rayDetectGround, true);

        foreach (RaycastHit2D hit in hits)
        {
            if(hit.collider != null)
                isOnGround = true;
        }
    }

    void drawLine()
    {
        Line.positionCount = 2;
        Line.SetPosition(0,this.transform.position);



        Vector2 tmpPos = Vector2.zero;
        tmpPos.x = Mathf.Abs(lineLength) * Mathf.Cos(angle);
        tmpPos.y = Mathf.Abs(lineLength) * Mathf.Sin(angle);

        RaycastHit2D hit = Physics2D.Raycast(this.transform.position, tmpPos - (Vector2)this.transform.position, Mathf.Infinity);

        if (hit.collider != null)
            Line.SetPosition(1, hit.point);
        else
            Line.SetPosition(1, tmpPos + (Vector2)this.transform.position);
    }

    //private void OnCollisionEnter2D(Collision2D collision)
    //{
    //    //if(collision.gameObject.CompareTag("gound"))
    //    isOnGround = true;
    //}

    
}
