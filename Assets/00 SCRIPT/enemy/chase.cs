using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class chase : MonoBehaviour
{
    [SerializeField] Transform player;
    [SerializeField] float size = 1;
    [SerializeField] float Speed = 1;
    [SerializeField] MovingPlatform patrolFunc;

    [SerializeField]
    GameObject target = null;
    // Start is called before the first frame update
    void Start()
    {
        patrolFunc = this.GetComponent<MovingPlatform>();
    }

    // Update is called once per frame
    void Update()
    {
        Chase();
        DetectTarget();
    }

    void Chase()
    {
        if(target == null)
        {
            patrolFunc.enabled = true;
            return;
        }

        patrolFunc.enabled = false;


        this.transform.position = Vector2.MoveTowards(this.transform.position, target.transform.position, Speed * Time.deltaTime);

    }

    void DetectTarget() {
        if (Vector2.Distance(this.transform.position, player.position) > size)
        {
            target = null;
            return;
        }
        Debug.DrawLine(this.transform.position, player.position, Color.red);

        Vector2 dir = player.position - this.transform.position;
        RaycastHit2D hit = Physics2D.Raycast(this.transform.position, dir, Mathf.Infinity);



        if (hit.collider != null)
        {
            if (hit.collider.CompareTag(CONSTANT.Player_tag))
            {
                target = player.gameObject;
                return;
            }

            target = null;
            
        }
        else
        {
            target = null;
        }


    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;

        Gizmos.DrawWireSphere(this.transform.position, size);

    }
}
