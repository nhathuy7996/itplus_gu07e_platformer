using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] float rayDetectGround = 1;
    [SerializeField] float speed = 0, jumpForce = 200;
    Vector2 movement;
    AnimControllerBase animController;
    Rigidbody2D RIGI;

    [SerializeField] PlayerState STATE = PlayerState.IDLE;

    [SerializeField] List<PhysicsMaterial2D> frictions = new List<PhysicsMaterial2D>();

    [SerializeField]
    float slopeForce = 10;
    bool isOnSlope = false;

    Vector2 SCALE;
    // Start is called before the first frame update
    void Awake()
    {
        SCALE = this.transform.localScale;
        if (animController == null)
            animController = this.transform.GetChild(0).GetComponent<AnimControllerBase>();
        RIGI = this.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {

        Moving();

        UpdateState();

        Vector2 scale = this.transform.localScale;


        scale.x = movement.x == 0 ? scale.x : (  movement.x > 0 ? Mathf.Abs(SCALE.x) : -Mathf.Abs(SCALE.x));

        //if(movement.x == 0)
        //{
        //   // scale.x = scale.x;
        //}else if(movement.x > 0)
        //{
        //    scale.x = Mathf.Abs(SCALE.x);
        //}else
        //{
        //    scale.x = -Mathf.Abs(SCALE.x);
        //}

        this.transform.localScale = scale;

        PlatformDetect();

        OnSlope();
    }

    void OnSlope()
    {
        if (!isOnSlope) return;

        if (RIGI.velocity.x > 0) return;

        Vector2 tmp = RIGI.velocity;
        tmp.y = -Mathf.Abs(slopeForce);

        RIGI.velocity = tmp;
    }

    void PlatformDetect()
    {
        Debug.DrawRay(this.transform.position, Vector2.down * rayDetectGround, Color.red);
        RaycastHit2D hit = Physics2D.Raycast(this.transform.position, Vector2.down, rayDetectGround);

        if (hit.collider != null)
        {
            Debug.DrawRay(hit.point, hit.normal, Color.yellow);

            if(this.transform.parent == null && hit.collider.GetComponent<MovingPlatform>() != null)
            {
                this.transform.SetParent(hit.collider.transform);
                SCALE = this.transform.localScale;
            }
            

            if (hit.normal != Vector2.up)
            {
                Debug.Log("player on slope");
                isOnSlope = true;
                
            }
            else
            {
                isOnSlope = false;
            }
            return; 
        }

        isOnSlope = false;

        this.transform.SetParent(null);
        SCALE = this.transform.localScale;
    }

    void Moving()
    {
        movement.y = RIGI.velocity.y;
        movement.x = InputController.getAxisRawHorizon() * speed * Time.deltaTime;
        RIGI.velocity = movement;

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if(STATE != PlayerState.JUMP && STATE != PlayerState.ON_AIR)
                RIGI.AddForce(new Vector2(0, jumpForce));
        }

        if (frictions.Count <= 1)
            return;
        if (InputController.getAxisRawHorizon() == 0)
            RIGI.sharedMaterial = frictions[1];
        else
            RIGI.sharedMaterial = frictions[0];
    }

    void UpdateState()
    {
        if (movement.y == 0 || isOnSlope )
        {
            if (movement.x != 0 && STATE != PlayerState.RUN)
            {
                STATE = PlayerState.RUN;
                animController.ChangeAnim(PlayerState.RUN);
            }

            else if (movement.x == 0 && STATE != PlayerState.IDLE)
            {
                STATE = PlayerState.IDLE;
                animController.ChangeAnim(PlayerState.IDLE);
            }
        }
        else
        {
            if (movement.y > 0)
            {
                if (STATE != PlayerState.JUMP)
                    animController.ChangeAnim(PlayerState.JUMP);
                STATE = PlayerState.JUMP;


            }
            else if (movement.y < 0)
            {

                if (STATE != PlayerState.ON_AIR && STATE != PlayerState.JUMP)
                    animController.ChangeAnim(PlayerState.JUMP);
                STATE = PlayerState.ON_AIR;
            }
        }

    }

    public enum PlayerState
    {
        IDLE,
        RUN,
        JUMP,
        ON_AIR,
        len
    }
}
